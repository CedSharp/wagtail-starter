FROM python:3.8
LABEL maintainer="cedsharp@cedsharp.ca"

COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev
ENV WAGTAIL_VERSION 2.10.2

RUN pip install --upgrade pip
RUN pip install "wagtail==${WAGTAIL_VERSION}"
RUN pip install gunicorn

WORKDIR /code/

RUN useradd wagtail
RUN chown -R wagtail /code
USER wagtail

EXPOSE 8000
ENTRYPOINT /docker-entrypoint.sh
