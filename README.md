# Wagtail Starter

I was tired of always setting up wagtail for new projects (especially for prototyping), so I created this repo
to make my life a little bit easier.

---

## First time setup

### Creating a Super User

Creation of a super user follows the wagtail documentation. Enter bash in the `wagtail` container
and run the typical wagtail `createsuperuser`

```shell script
$ docker-compose exec wagtail bash
$ python manage.py createsuperuser
```

---

## Development process

The image runs gunicorn on port 8000 with 3 workers. You should already be able to access http://localhost:8000/.
Changes to template should be reflected automatically as usual.