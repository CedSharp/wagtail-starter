set -ex;

if [ ! -f "/code/manage.py" ]; then
  cd /code;
  wagtail start wagtail_app .;
  rm -rf ./home ./search;
  sed '/search/d' ./wagtail_app/settings/base.py >> ./wagtail_app/settings/base2.py;
  sed '/home/d' ./wagtail_app/settings/base2.py >> ./wagtail_app/settings/base3.py;
  mv ./wagtail_app/settings/base3.py ./wagtail_app/settings/base.py;
  rm ./wagtail_app/settings/base2.py;
  sed '/search/d' ./wagtail_app/urls.py >> ./wagtail_app/urls2.py;
  mv ./wagtail_app/urls2.py ./wagtail_app/urls.py;
  python manage.py migrate;
fi

cd /code
exec gunicorn wagtail_app.wsgi:application --bind 0.0.0.0:8000 --workers 3